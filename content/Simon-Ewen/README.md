---

prenom: Ewen

nom: Simon

# écrit ci-dessous ta présentation en markdown

---

# Profil
![Ewen Simon](https://www.shutterstock.com/image-vector/waves-pixel-art-icon-water-250nw-1247138806.jpg)

**Ewen Simon, Etudiant en BTS SIO**

Je m'appelle Ewen Simon je suis actuellement en 1er année de mon BTS SIO (Service Informatique aux Organisation). Je suis passionné par l'informatique en particulier dans les domaines de la cyber sécurité et du développement logiciel je souhaite en apprendre toujours plus sur ces sujets. J'espère pouvoir échangez avec vous si vous êtes intéresser par mon profil.

Cordialement 
Ewen Simon

## Contact

Pour me contacter n'hésiter pas à utiliser les informations si dessous :

Email : [ewen.simon72@gmail.com](mailto:ewen.simon72@gmail.com)

Numéro de Téléphone : [07.82.45.44.23](tel:0782454423)

# Formations

* Mon parcours :

    1. **BAC STI2D** avec option Système d'Information et Numérique (**SIN**) _2021_/_2023_

    2. **BTS SIO** _2023_/_2025_

# Compétences

|Domaine|Exemple
|:-|:-:|
| Langage de programmation | Python, C/C++,html ,css
| Système d'exploitation | Windows, Linux             
| Langue | Français, Anglais
| Divers  | Travail d'équipe, Esprit critique          
        

# Expérience

#### Stage de 3eme :
Chez Solvarea à Nantes : 
SOLVAREA est une entreprise du groupe BOULANGER spécialisée dans la réparation d'appareils électrodomestiques.Ce stage ma permie de découvrir la variété de poste présent dans une entreprise, de la communication avec le client, au différent atelier de reparation, jusqu'à la logistique pour renvoyer le produit au client. 
