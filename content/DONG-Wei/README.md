---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Wei
nom: DONG

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---
# Profil

![photo](media/Dong-Wei.webp)

Bonjour, je m'appelle Wei DONG, j'ai 32 ans, origine de Shanghai, CHINE. J'etais un technicien informatique dans l'entreprise publique pour fournir le service sécurité au client. Je suis venu en France car la raison familliale. Je suis actuellement un étudiant de BTS SIO (Services informatiques aux organisations) et je voudrais choisir SISR (Solutions d'infrastructure, systèmes et réseaux) à la fin de ce semestre dans le lycée la Colinière. J'ai choisi le BTS SIO parce que j'aimerais travailler dans le domaine informatique dans le futur.



## CONTACT

**Téléphone: 0649301660**
**Courriel: weidaniel167@gmail.com**
**Linkedin: https://fr.linkedin.com/in/wei-dong-4a3a51147**

# FORMATION

**09/2022 – 06/2023 BAC PRO Système Numérique, Option RISC (Réseaux Informatiques et Systèmes Communicants)**<br>
_Formation réalisée chez GRETA – CFA Loire Atlantique
Diplôme à obtenir en Novembre 2023_


**01/2019 – 05/2020 Cours intensifs de langue Française**<br>
_Diplôme Universitaire d'Études Françaises, Université de Nantes, France (DUEF B1)_

**09/2012 – 09/2015 Licence en English Business Management**<br>
_Shanghai Zhongqiao Vocational and Technical College à Shanghai, Chine_

# COMPÉTENCES

| Catégorie     | Description |
| ----------- | ----------- |
| Informatique      | Diagnostic et localisation des pannes sur l'ordinateur |
| Logiciels et matériaux   | Système Installation; logiciels Installation; martériaux installation         |
|Réseau       | Configurer l'adresse IP et le domaine propre pour les bureaux
|Programmation       | Les connaisences basique sur la langage Python,Java et HTML5


# EXPÉRIENCES

**01/2023 – 03/2023 Stage en Maintenance Informatique et Gestion de la Clientèle**
Stage réalisé chez Atlantic Micro (31, Rue du Général Buat, 44000 Nantes) Stage obligatoire dans le cadre de la formation chez GRETA – CFA Loire Atlantique

**09/2021 – 09/2022 Réalisation des missions chez "Solidarité Emploi Association"**
Missions de travail diverses de courte durée (chez des particuliers ou entreprises) ou
de longue durée (dans des associations, ex. chez CREPS)

**09/2015 – 09/2018 Technicien de maintenance informatique**
30th Weishi Information Security Limited à Shanghai, Chine
